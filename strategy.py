#coding=utf-8
__author__ = 'linchao'

import math
import datetime

import pandas


start = datetime.date(2016, 8, 10)  # 回测起始时间
end  = datetime.date(2016, 8, 23)  # 回测结束时间
benchmark = 'IF'     # 策略参考标准，对冲基准股指期货
"""
前三个的数值为股指期货数据（对应股指期货未发布前用指数走势替代）
由于没有对应的股指期货产品，其余的为指数走势替代股指期货数据

IF
沪深300

IC
中证500

IH
上证50

000903.SH
中证100

000852.SH
中证1000

备注：若填写其他参数，则默认为沪深300股指期货进行对冲
"""

# universe = ['000002.sz','000001.sz','000004.sz']	# 股票池
capital_base = 1e6     # 起始资金
short_capital = 1e6  # 做空资金
# commission = Commission(0.001,0.001)
refresh_rate = 5  # 调用频率
freq = 'd'  # 时间单位

# window_short = 20
# window_long = 120
# longest_history = window_long
# SD = 0.05

universe_code = 'A'
self_defined = False
#universe_code = ['000002.SZ','000001.SZ','000004.SZ']  # 股票池
#self_defined = True
'''
000002.SH
上证A指

000300.SH
沪深300

000010.SH
上证180

000016.SH
上证50

000903.SH
中证100

399330.SZ
深证100

399312.SH
国证300

399001.SZ
深证成指

A
全部A股

000905.SH
中证500

000906.SH
中证800

000852.SH
中证1000
'''
# sql1 = "select * from new_stock_index WHERE group_code = '%s'and date_time BETWEEN '%s' and '%s' "%(universe_code,start,end)
# universe_temp = pd.read_sql(sql1,conn)
# universe_set = set([])
# for item in universe_temp['content']:
#     universe_set.update(item.split(','))
# print len(universe_set)
# all_universe = list(universe_set)

latest_universe = []


# def initialize(account):					# 初始化虚拟账户状态
#     # account.fund = universe[0]
#     account.SD = 0.1
#     account.window_short = 20
#     account.window_long = 120
#     account.f1=0.05
#     account.f2=0.2
#     account.f3=0.3
#     #account.hangup=[]
#
# def stop_loss(account):
#     for i in account.valid_secpos.index[account.valid_secpos>0]:
#         todayHigh=account.get_high_low(i, _high=True, _today=True)
#         todayLow=account.get_high_low(i, _high=False, _today=True)
#         todayOpen=account.get_price(i, _open=True, _today=True)
#         stopprice=account.avgBuyprice[i]*(1-account.f1)
#         upprice=account.avgBuyprice[i]*(1+account.f2)
#         old_buy=account.buyPosition[i]
#         if old_buy<account.days_counts:
#             HighSinceBuy=account.getDailyHistory(i,account.days_counts-old_buy)['high'].max()
#             if stopprice>=todayLow:
#                 if todayOpen>=stopprice:
#                     account.order_to(i,0,stopprice)
#                 else:
#                     account.order_to(i,0,todayOpen)
#             #注释掉止盈的代码，f2和f3需要修改
#             #if HighSinceBuy>=upprice and todayLow<HighSinceBuy*(1-account.f3):
#             #    if account.isOk2order(i):
#             #        account.order_to(i,0,stopprice)

def initialize(account):					# 初始化虚拟账户状态
    # account.fund = universe[0]
    account.SD = 0.1
    account.window_short = 20
    account.window_long = 120
    account.f1=0.05
    account.f2=0.08
    account.f3=0.3
    #account.hangup=[]

def stop_loss(account):
    for i in account.valid_secpos.index[account.valid_secpos>0]:
        todayHigh=account.get_high_low(i, _high=True, _today=True)
        todayLow=account.get_high_low(i, _high=False, _today=True)
        todayOpen=account.get_price(i, _open=True, _today=True)
        stopprice=account.avgBuyprice[i]*(1-account.f1)
        upprice=account.avgBuyprice[i]*(1+account.f2)
        old_buy=account.buyPosition[i]
        if old_buy<account.days_counts:
            HighSinceBuy=account.getDailyHistory(i,account.days_counts-old_buy)['high'].max()
            if stopprice>=todayLow:
                if todayOpen>=stopprice:
                    account.order_to(i,0,stopprice)
                else:
                    account.order_to(i,0,todayOpen)
            #注释掉止盈的代码，f2和f3需要修改
            #if HighSinceBuy>=upprice and todayLow<HighSinceBuy*(1-account.f3):
            #    if account.isOk2order(i):
            #        account.order_to(i,0,stopprice)


        # if old_buy<account.days_counts:
        #     # HighSinceBuy=account.getDailyHistory(i,account.days_counts-old_buy)['high'].max()
        #     if stopprice>=todayLow:
        #         if todayOpen>=stopprice:
        #             account.order_to(i,0,stopprice)
        #         else:
        #             account.order_to(i,0,todayOpen)
        #     if upprice<=todayHigh:
        #         if todayOpen<=upprice:
        #             account.order_to(i,0,upprice)
        #         else:
        #             account.order_to(i,0,todayOpen)

def can_deal_four(account):
    account.charge_num += 1
    num = account.charge_num
    num -= 1
    if num % 3 == 0:
        print 'go for it!'
        return True
    return False


def deal_two():
    return True


def handle_data(account, strategy_func=None):
    """
    """
    # 每天处理停牌和跌停的股票
    account.order_suspension_and_down_limit()
    if account.days_counts == 1:
        return

    # 交易周期为1天
    if deal_two():
        # if account.is_my_deal_day(type='monthly', skip_time=datetime.date(1, 1, 1), run_time=datetime.date(1, 1, 1)):
        if self_defined:
            latest_universe = universe_code
        else:
            latest_universe = account.get_latest_universe()
            # 处理历史上在沪深300中,但是这个月不在沪深300中的股票列表
        to_be_selled = set(account.all_universe).difference(set(latest_universe))
        for stock_id in to_be_selled:
            # if account.isOk2order(stock_id):
            today_open = account.get_price(stock_id, _open=True, _today=True)
            # 把所有在account.all_universe中但是不在latest_universe中的股票一一卖掉
            account.order_to(stock_id, 0, today_open)
        # 处理股票
        process_stocks(account, latest_universe, strategy_func)
    else:
        # 止损, 在没有换仓的时候运行
        # 由于股票市场存在T+1的情况新增逻辑判断不能对今天买入的股票进行止盈止损
        stop_loss(account)

def process_stocks(account, latest_universe, strategy_func=None):
    """
    说明,这是一个简单的测试策略,用户可以根据需求自定义策略
    1. 对新的股票列表进行筛选,得到这个月第一个交易日的开盘价大于上
    个月最后一天的收盘价, 这样就得到了一个待下单的股票列表
    2. 对下单的股票列表的每个元素分别计算要下单的手数
    """
    # 每次换仓前，候选池要减去停牌，跌停以及st的股票
    # 得到正确的股票候选池
    account.delete_exceptions(suspension=True, down=True, up=True, st=True)
    # 选股策略
    # 选出的股票列表
    reselected_stocks = strategy(account, strategy_func)
    # backup account.valid_secpos[account.valid_secpos>0]
    # 之前选出的股票
    old_reselected_stocks = account.valid_secpos[account.valid_secpos>0]
    # 止盈止损,在处理旧股票之前需要进行运行，不然没有意义。（特别天换仓的）
    # stop_loss(account)
    # 卖掉account.valid_secpos中没有出现在reselected_stocks中的股票,这里会记录停牌和跌停股票
    process_old_stocks(account, reselected_stocks)
    # 处理停牌和跌停的股票资产(如果历史记录的股票出现了停牌或跌停)
    active_capital = account.process_suspension_and_down_limit()
    # 得到股票的资本分配
    stock_capital = account.allocate_capital(active_capital, reselected_stocks, strategy=1)
    # 调用order_to
    deal_new_stock(account, stock_capital)
    # 记录换手率，并且买卖
    account.calculate_turnover_rate(stock_capital, old_reselected_stocks)

def strategy(account, strategy_func=None):
    """
    选股策略
    简单策略：对新的股票列表进行筛选,得到这个月第一个交易日的开盘价大于上
            　个月最后的交易日的收盘价的股票
    """
    # 由于上面的逻辑已经处理了停牌，跌停以及st，因此不会选到停牌跌停股
    reselected_stocks = []
    if account.resume_system == True and\
       str(account.current_time) in account.resume_times.tolist():
        account._temp_data = account.backup_data[account.backup_data['Time']==str(account.current_time)]
        reselected_stocks = account._temp_data['Code'].tolist()
    else:
        reselected_stocks = strategy_func(account)
    # 直观输出选股情况
    print "当前时间 %s" % account.current_time
    print "选股数目 %s" % len(reselected_stocks)
    print "选股代码 %s" % reselected_stocks
    # 将选股情况保存在./data/output_data/chosen_stock.txt中
    with open("./data/output_data/chosen_stock.txt", "a") as file:
        file.write(str(account.current_time) + "  ")
        for stock_id in reselected_stocks:
            file.write(stock_id + " ")
        file.write("\n")
    return reselected_stocks

def process_old_stocks(account, reselected_stocks):
    """
    卖掉account.valid_secpos中没有出现在reselected_stocks中的股票,这里会记录停牌和跌停股票
    """
    to_be_selled = set(account.valid_secpos.index.tolist()).difference(set(reselected_stocks))
    for stock_id in to_be_selled:
        today_open = account.get_price(stock_id, _open=True, _today=True)
        # 这里可能会出错，如果today_open 为nan,那么self.cash会为nan
        # 这里的的股票需要等待数据不为空的时候再卖出
        account.order_to(stock_id, 0, today_open)

def deal_new_stock(account, stock_capital):
    """
    对这次选股进行交易
    """
    print '-------------------------------------deal new stock start-------------------------------------'
    account.sell_first_order_to(stock_capital, priority_time=datetime.date(1, 1, 1))
    print "总资产为%s" % account.dynamic_record['capital'][-1]
    print "可用资产为%s" % account.dynamic_record['cash'][-1]
    print "买入金额："
    print stock_capital
    print '--------------------------------------deal new stock end--------------------------------------'
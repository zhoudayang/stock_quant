# coding=utf-8
import os
import time
import datetime
import multiprocessing

import pandas
import MySQLdb
import scipy.io

import config.config_parser
import utils.matlab2py


class Data(object):
    """"""

    def __init__(self):
        """
        >>> import data.system_data.data
        >>> system_data = data.system_data.data.Data()
        """
        self.system_config = config.config_parser.Parser()
        self.connection = DBConnection()


class StockData(Data):
    """"""

    def __init__(self, freq, benchmark_code, start, end, self_defined, universe_code):
        """"""
        self.freq = freq
        self.start = start
        self.end = end
        self.days = None
        self.self_defined = False
        self.benchmark_code = benchmark_code
        self.benchmark_data = None
        self.universe_code = universe_code
        Data.__init__(self)

    def get_time_list(self):
        """"""
        _date_query = "SELECT * FROM `tradeday` WHERE SH_tradeday >= '%s' AND SH_tradeday < '%s'" \
                      % (self.start, self.end)
        time_list = pandas.read_sql(_date_query, self.connection.conn)['SH_tradeday']
        self.days = time_list.count()
        return time_list

    def get_all_universe(self):
        """"""
        if self.self_defined:
            all_universe = self.universe_code
        elif self.universe_code == 'A':
            _query = "SELECT * FROM A_stockName"
            _temp = pandas.read_sql(_query, self.connection.conn)
            all_universe = set(_temp['wind_code'])
            _query = "SELECT * FROM my_stock_index WHERE group_code = '%s' AND date < '%s' " \
                     % (self.universe_code, self.end)
            universe_temp = pandas.read_sql(_query, self.connection.conn)
        else:
            _query = "SELECT * FROM my_stock_index WHERE group_code = '%s' AND date < '%s' " \
                     % (self.universe_code, self.end)
            universe_temp = pandas.read_sql(_query, self.connection.conn)
            _universe_set = set([])
            for item in universe_temp['content']:
                _universe_set.update(item.split(','))
            all_universe = list(_universe_set)
        return all_universe, universe_temp

    def get_benchmark_data(self):
        """"""
        if self.benchmark_code == 'IF':
            _table = 'hushen300'
        elif self.benchmark_code == "IC":
            _table = 'zhongzheng500'
        elif self.benchmark_code == "IH":
            _table = 'shangzheng50'
        elif self.benchmark_code == "000903.SH":
            _table = 'zhongzheng100'
        elif self.benchmark_code == "000852.SH":
            _table = 'zhongzheng1000'
        else:
            # 备注：输入其余则默认为沪深300
            _table = 'hushen300'
        _query = "SELECT * FROM `%s` WHERE date >= '%s' AND date < '%s' "\
                %(_table, self.start, self.end)
        return pandas.read_sql(_query, self.connection.conn)

    def get_suspened_stocks(self):
        """"""
        _query = "SELECT DATE(date), wind_code as code FROM `A_stock_suspend` WHERE date>='%s' AND date<'%s'" \
                 % (self.start, self.end)
        _result = pandas.read_sql(_query, self.connection.conn)
        _result['DATE(date)'] = _result['DATE(date)'].map(date2Timestamps)
        return _result

    def get_ST_stocks(self):
        """"""
        _query = "SELECT DATE(date), wind_code from `A_stock_st` WHERE date>='%s' AND date<'%s'" \
                 % (self.start, self.end)
        _result = pandas.read_sql(_query, self.connection.conn)
        _result['DATE(date)'] = _result['DATE(date)'].map(date2Timestamps)
        return _result

    def get_data(self, all_universe):
        """
        获取基本数据
        """
        data, dfs = {}, []
        data = multiprocessing.Manager().dict()
        limit, offset = 100000, 0
        self.connection.conn.select_db('A_stock_day')
        _s = time.time()
        while True:
            _query = "SELECT wind_code as Code, date as date_time, open, high, low, close, amt, volume" \
                     " FROM A_stock_basic WHERE date >= '%s' and date < '%s' LIMIT %d OFFSET %d" \
                     % (self.start, self.end, limit, offset)
            dfs.append(pandas.read_sql(_query, self.connection.conn))
            offset += limit
            if len(dfs[-1]) < limit:
                break
        _e = time.time()
        print 'read A_stock_day: ', _e - _s
        _a_stock_basic_data = pandas.concat(dfs)

        def parallel_func(_all_universe, stock_data):
            for _code in _all_universe:
                _temp_data = stock_data[stock_data['Code'] == _code]
                data[_code] = _temp_data[_temp_data.notnull()]

        _s = time.time()
        _processes, all_universe = [], list(all_universe)
        _p_num = 4
        for _i in range(_p_num):
            _st, _end = _i * len(all_universe) / 10, (_i + 1) * len(all_universe) / 10
            _p = multiprocessing.Process(target=parallel_func, \
                                         args=(all_universe[_st:_end if _i != _p_num - 1 else len(all_universe)],
                                               _a_stock_basic_data,))
            _processes.append(_p)
        for _p in _processes:
            _p.start()
        for _p in _processes:
            _p.join()
        _e = time.time()
        print 'time costs', _e - _s
        self.connection.conn.select_db(self.connection.system_config.get('db', 'db_default'))
        return data

    def get_data_fuquan(self, all_universe):
        """
        获取复权后的基本数据
        """
        _data_fuquan, dfs = {}, []
        limit, offset = 100000, 0
        _data_fuquan = multiprocessing.Manager().dict()
        self.connection.conn.select_db('A_stock_day')
        _s = time.time()
        while True:
            _query = "SELECT wind_code as Code, date as date_time, open, high, low, close, amt, volume" \
                     " FROM A_stock_basic_fuquan WHERE date >= '%s' and date < '%s' LIMIT %d OFFSET %d" \
                     % (self.start, self.end, limit, offset)
            dfs.append(pandas.read_sql(_query, self.connection.conn))
            offset += limit
            if len(dfs[-1]) < limit:
                break
        _e = time.time()
        print 'read A_stock_day_fuquan: ', _e - _s
        _a_stock_fuquan_data = pandas.concat(dfs)

        def parallel_func(_all_universe, stock_data):
            for _code in _all_universe:
                _temp_data = stock_data[stock_data['Code'] == _code]
                _data_fuquan[_code] = _temp_data[_temp_data.notnull()]

        _s = time.time()
        _processes, all_universe, _p_num = [], list(all_universe), 4
        for _i in range(_p_num):
            _st, _end = _i * len(all_universe) / 10, (_i + 1) * len(all_universe) / 10
            _p = multiprocessing.Process(target=parallel_func, \
                                         args=(all_universe[_st:_end if _i != _p_num - 1 else len(all_universe)],
                                               _a_stock_fuquan_data,))
            _processes.append(_p)
        for _p in _processes:
            _p.start()
        for _p in _processes:
            _p.join()
        _e = time.time()
        print 'time costs:', _e - _s
        self.connection.conn.select_db(self.connection.system_config.get('db', 'db_default'))
        return _data_fuquan

    def get_market_capital(self):
        """"""
        _query = "SELECT wind_code as Code, mkt_cap_ard as Mkt_cap FROM `A_stock_market_value` " \
                 " WHERE date >= '%s' and date < '%s'" % (self.start, self.end)
        self.connection.conn.select_db(self.connection.system_config.get('db', 'db_default'))
        return pandas.read_sql(_query, self.connection.conn)

    def get_A_stock_market_value(self, wind_code, start, end, ev=False, free_float_shares=False,
                                 mkt_cap_ard=False, mkt_cap_float=False, mkt_freeshares=False):
        """
        >>> import data.system_data.data
        >>> stock = data.system_data.data.StockData('d', 'A', '2016-03-31 00:00:00', '2016-04-01 00:00:00', False, 'A')
        >>> wind_code, df = stock.get_A_stock_market_value('000001.SZ','2016-03-31 00:00:00', '2016-04-01 00:00:00',
        ... ev=True, mkt_cap_ard=True)
        >>> wind_code
        '000001.SZ'
        >>> df
           wind_code       date            ev   mkt_cap_ard
        0  000001.SZ 2016-03-31  1.522443e+11  1.522443e+11
        1  000001.SZ 2016-04-01  1.525305e+11  1.525305e+11

        [2 rows x 4 columns]
        """
        _params = []
        if ev:
            _params.append('ev')
        if free_float_shares:
            _params.append('free_float_shares')
        if mkt_cap_ard:
            _params.append('mkt_cap_ard')
        if mkt_cap_float:
            _params.append('mkt_cap_float')
        if mkt_freeshares:
            _params.append('mkt_freeshares')
        _str_params = str(_params)[1:-1].replace("'", "")
        _query = "SELECT `wind_code`, `date`, %s FROM `A_stock_market_value` WHERE `date` >= '%s' AND `date` <= '%s'" \
                 " AND `wind_code` = '%s'" % (_str_params, start, end, wind_code)
        return wind_code, pandas.read_sql(_query, self.connection.conn)

    def get_A_stock_annue_day(self, wind_code, start, end,
                              index=False, est_peg=False, ev2_to_ebitda=False, pb=False, pcf_ncf=False, pe=False,
                              pe_est=False, ps=False, west_eps_FY1=False,west_stdroe=False, dividendyield2=False):
        """
        >>> import data.system_data.data
        >>> stock = data.system_data.data.StockData('d', 'A', '2016-03-31 00:00:00', '2016-04-01 00:00:00', False, 'A')
        >>> wind_code, df = stock.get_A_stock_annue_day('000001.SZ', '2016-03-30 00:00:00', '2016-04-01 00:00:00',
        ... index=True, est_peg=True, ev2_to_ebitda=True)
        >>> wind_code
        '000001.SZ'
        >>> df
           wind_code       date index   est_peg ev2_to_ebitda
        0  000001.SZ 2016-03-30  None  0.734484          None
        1  000001.SZ 2016-03-31  None  0.730365          None
        2  000001.SZ 2016-04-01  None  0.731738          None

        [3 rows x 5 columns]
        """
        _params = []
        if index:
            _params.append('index')
        if est_peg:
            _params.append('est_peg')
        if ev2_to_ebitda:
            _params.append('ev2_to_ebitda')
        if pb:
            _params.append('pb')
        if pcf_ncf:
            _params.append('pcf_ncf')
        if pe:
            _params.append('pe')
        if pe_est:
            _params.append('pe_est')
        if ps:
            _params.append('ps')
        if west_eps_FY1:
            _params.append('west_eps_FY1')
        if west_stdroe:
            _params.append('west_stdroe')
        if dividendyield2:
            _params.append('dividendyield2')
        _new_params = ''
        for _param in _params:
            _s = '`%s`' % _param
            _new_params += ',' + _s
        _query = "SELECT `wind_code`, `date`, %s FROM `A_stock_annue_day` WHERE `date` >= '%s' AND `date` <= '%s'" \
                 " AND `wind_code` = '%s'" % (_new_params[1:], start, end, wind_code)
        return wind_code, pandas.read_sql(_query, self.connection.conn)

    def get_A_stock_quarter_2016(self, wind_code, start, end,  *args):
        """
        Parameter
        ---------
        wind_code: 股票代码，如'000001.SZ'
        start: 开始时间，如'2016-01-01 00:00:00'
        end: 结束时间，如'2016-01-02 00:00:00'
        args:
            index, wind_code, date, acct_rcv, acct_payable, adminexpensetogr, debttoassets,
            finaexpensetogr, monetary_cap, operateexpensetogr, salescashintoor, tot_assets,
            arturn, assetsturn, bps, cashtocurrentdebt, cfps, current, currentdebttodebt,
            eps_basic, fin_exp_is, grossprofitmargin, invturn, longdebttodebt, net_cash_flows_oper_act,
            net_profit_is, netprofitmargin, optoebt, quick, roa, roe, tot_cur_liab, tot_liab,
            tot_oper_rev, tot_profit, wgsd_assets, wgsd_stkhldrs_eq, yoy_or, yoy_equity,
            yoyprofit, growth_totalequity

        usage
        -----
        wind_code, start, end参数是必须填写的，其他的指标你需要以字符串的方式传入，如:
            get_A_stock_quarter_2016('000001.SZ', '2006-03-31 00:00:00', '2015-03-31 00:00:00', 'acct_rcv', 'eps_basic')

        注意
        ---
        - 指标别写错
        - 注意时间的范围
        """
        _new_params = ''
        for _param in list(args):
            _s = '`%s`' % _param
            _new_params += ',' + _s
        _query = "SELECT `wind_code`, `date`, %s FROM `A_stock_quarter_2016` WHERE `date` >= '%s' AND `date` <= '%s'" \
                 " AND `wind_code` = '%s'" % (_new_params[1:], start, end, wind_code)
        return wind_code, pandas.read_sql(_query, self.connection.conn)


class IndexFuturesData(Data):
    """"""

    def __init__(self, start, end, benchmark_code):
        """"""
        self.start = start
        self.end = end
        self.benchmark_code = benchmark_code
        Data.__init__(self)

    def get_futures_data(self):
        """"""
        if self.benchmark_code == "IF":
            _name = 'hushen300_IF00'
        elif self.benchmark_code == "IC":
            _name = 'zhongzheng500_IC00'
        elif self.benchmark_code == "IH":
            _name = 'shangzheng50_IH00'
        elif self.benchmark_code == "000903.SH":
            _name = 'zhongzheng100'
        elif self.benchmark_code == "000852.SH":
            _name = 'zhongzheng1000'
        else:
            _name = 'hushen300_IF00'
        _futures_query= "SELECT Date, Open, High, Low, Close FROM `%s` WHERE Date>='%s 00:00:00' AND Date<'%s 00:00:00'"\
                        " order by Date" % (_name, self.start, self.end)
        data_futures = pandas.read_sql(_futures_query, self.connection.conn)
        return data_futures

    # def get_IC_data(self):
    #     """"""
    #     _ic_query = "SELECT wind_code as Code, date as Date, open as Open, high as High,"\
    #         " low as Low, close as Close, volume as Volume, amt as Amt, turn as Trun, "\
    #         " free_float_shares as Free_float_shares, ev as Ev, mkt_cap_ard as Mkt_cap_ard,"\
    #         " mkt_freeshares as Mkt_freeshares FROM zhongzheng500_IC00 WHERE date >= '%s 00:00:00' AND date < '%s 00:00:00'"\
    #         %(self.start, self.end)
    #     return pandas.read_sql(_ic_query, self.connection.conn)
    #
    # def get_IH_data(self):
    #     """"""
    #     _ih_query = "SELECT wind_code as Code, date as Date, open as Open, high as High,"\
    #         " low as Low, close as Close, volume as Volume, amt as Amt, turn as Trun, "\
    #         " free_float_shares as Free_float_shares, ev as Ev, mkt_cap_ard as Mkt_cap_ard,"\
    #         " mkt_freeshares as Mkt_freeshares FROM shangzheng50_IH00 WHERE date >= '%s 00:00:00' AND date < '%s 00:00:00'"\
    #         %(self.start, self.end)
    #     return pandas.read_sql(_ih_query, self.connection.conn)

    def get_futures_delivery_info(self):
        """"""
        self.deliveryInfo = scipy.io.loadmat(os.getcwd() + '/data/system_data/deliveryInfo.mat')
        return [utils.matlab2py.matlab2datetime(_v[0]) for _v in self.deliveryInfo['deliveryInfo'][0][0][1]]


class IndustryData(Data):
    """"""

    def __init__(self, start, end):
        """"""
        self.start = start
        self.end = end
        Data.__init__(self)

    def get_citic(self):
        """"""
        _class_query = "SELECT * FROM A_stock_industry_citic"
        _citic_1_query = "SELECT wind_code as Code, date as Date, industry_citiccode_1," \
                         " industry_citic_1 FROM A_stock_industry_citic_1 WHERE date >= '%s' AND date < '%s'" \
                         % (self.start, self.end)
        _citic_2_query = "SELECT wind_code as Code, date as Date, industry_citiccode_2," \
                         " industry_citic_2 FROM A_stock_industry_citic_2 WHERE date >= '%s' AND date < '%s'" \
                         % (self.start, self.end)
        self.connection.conn.select_db(
            self.connection.system_config.get('industry', 'industry_db'))
        _citic_1 = pandas.read_sql(_citic_1_query, self.connection.conn)
        _citic_2 = pandas.read_sql(_citic_2_query, self.connection.conn)
        a_stock_indus_CITI_class = pandas.read_sql(_class_query, self.connection.conn)
        a_stock_indus_CITI = _citic_1.merge(_citic_2, on=['Code', 'Date'])
        return a_stock_indus_CITI_class, a_stock_indus_CITI

    def get_csrc(self):
        """"""
        _class_query = "SELECT * FROM A_stock_industry_csrc"
        _csrc_1_query = "SELECT wind_code as Code, date as Date, industry_CSRCcode12_1," \
                        " industry_CSRC12_1 FROM A_stock_industry_csrc_1 WHERE date >= '%s' AND date < '%s'" \
                        % (self.start, self.end)
        _csrc_2_query = "SELECT wind_code as Code, date as Date, industry_CSRCcode12_2," \
                        " industry_CSRC12_2 FROM A_stock_industry_csrc_2 WHERE date >= '%s' AND date < '%s'" \
                        % (self.start, self.end)
        self.connection.conn.select_db(
            self.connection.system_config.get('industry', 'industry_db'))
        _csrc_1 = pandas.read_sql(_csrc_1_query, self.connection.conn)
        _csrc_2 = pandas.read_sql(_csrc_2_query, self.connection.conn)
        a_stock_indus_CSRC_class = pandas.read_sql(_class_query, self.connection.conn)
        a_stock_indus_CSRC = _csrc_1.merge(_csrc_2, on=['Code', 'Date'])
        return a_stock_indus_CSRC_class, a_stock_indus_CSRC

    def get_sw(self):
        """"""
        _class_query = "SELECT * FROM A_stock_industry_sw"
        _sw_1_query = "SELECT wind_code as Code, date as Date, industry_swcode_1," \
                      " industry_sw_1 FROM A_stock_industry_sw_1 WHERE date >= '%s' AND date < '%s'" \
                      % (self.start, self.end)
        _sw_2_query = "SELECT wind_code as Code, date as Date, industry_swcode_2," \
                      " industry_sw_2 FROM A_stock_industry_sw_2 WHERE date >= '%s' AND date < '%s'" \
                      % (self.start, self.end)
        self.connection.conn.select_db(
            self.connection.system_config.get('industry', 'industry_db'))
        _sw_1 = pandas.read_sql(_sw_1_query, self.connection.conn)
        _sw_2 = pandas.read_sql(_sw_2_query, self.connection.conn)
        a_stock_indus_SW_class = pandas.read_sql(_class_query, self.connection.conn)
        a_stock_indus_SW = _sw_1.merge(_sw_2, on=['Code', 'Date'])
        return a_stock_indus_SW_class, a_stock_indus_SW


class Quota(Data):
    """"""

    def __init__(self):
        """"""
        Data.__init__(self)

    def get_quota(self, current_time, quota_name, quarter_or_days=True):
        """
        >>> import datetime
        >>> import data.system_data.data
        >>> quota_class = data.system_data.data.Quota()
        >>> _pb, _dict_pb = quota_class.get_quota(datetime.date(2015, 2, 2), 'pb', quarter_or_days=False)
        >>> _roe, _dict_roe = quota_class.get_quota(datetime.date(2015, 2, 2), 'roe', quarter_or_days=True)
        """
        dict_data = {}
        self.connection.conn.select_db('update')
        if quarter_or_days == True:
            _month = current_time.strftime("%m")
            if _month == "01" or _month == "11" or _month == "12" or _month == "02" or _month == "03" or _month == "04":
                if _month == "01" or _month == "02" or _month == "03" or _month == "04":
                    _year = int(current_time.strftime("%Y")) - 1
                else:
                    _year = int(current_time.strftime("%Y"))
                _query = "select wind_code,%s from `A_stock_quarter` where date = '%s'" \
                        % (quota_name, current_time.strftime("%s-09-30 00:00:00" % _year))
            elif _month == "05" or _month == "06" or _month == "07" or _month == "08":
                _year = int(current_time.strftime("%Y")) - 1
                _query = "select wind_code,%s from `A_stock_quarter` where date = '%s'" \
                        % (quota_name, current_time.strftime("%s-12-31 00:00:00" % _year))
            else:
                _year = int(current_time.strftime("%Y"))
                _query = "select wind_code,%s from `A_stock_quarter` where date = '%s'" \
                        % (quota_name, current_time.strftime("%s-06-30 00:00:00" % _year))
            data = pandas.read_sql(_query, self.connection.conn)
        else:
            _query = "select wind_code,%s from `A_stock_annue_day` where date = '%s'" \
                    % (quota_name, current_time.strftime("%Y-%m-%d 00:00:00"))
            data = pandas.read_sql(_query, self.connection.conn)
        dict_data = {_v[0]: _v[1] for _v in zip(data['wind_code'], data[quota_name])}
        return data, dict_data


class RuntimeData(StockData, IndexFuturesData, IndustryData, Quota):
    """
    运行数据
    """

    def __init__(self, freq, benchmark_code, start, end, self_defined, universe_code):
        """"""
        StockData.__init__(self, freq, benchmark_code, start, end, self_defined, universe_code)
        IndexFuturesData.__init__(self, start, end, benchmark_code)
        IndustryData.__init__(self, start, end)
        Quota.__init__(self)


class DBConnection():
    """"""

    def __init__(self):
        """"""
        self.system_config = config.config_parser.Parser()
        self.conn = MySQLdb.connect(
            host=self.system_config.get('db', 'db_host'),
            user=self.system_config.get('db', 'db_user'),
            passwd=self.system_config.get('db', 'db_passwd'),
            db=self.system_config.get('db', 'db_default'),
            charset=self.system_config.get('db', 'db_charset')
        )
        self.cursor = self.conn.cursor()

    def __del__(self):
        """"""
        self.cursor.close()
        self.conn.close()


def date2Timestamps(t):
    """
    convert datetime.date type to pandas.Timestamp type
    """
    return pandas.Timestamp(t)
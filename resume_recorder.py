#!/usr/bin/env python
# coding=utf-8

import os
import subprocess

import pandas


class ResumeRecorder(object):
    """"""
    def __init__(self):
        """"""
        try:
            subprocess.cal(['rm', './data/system_backup/selected_stocks.csv'], shell=False)
        except Exception, e:
            pass
        self.data = pandas.DataFrame({}, columns=[])
        self.selected_stocks = pandas.DataFrame({}, columns=['Time', 'Code', 'Capital'])

    def add_data(self, stock_capital, current_time):
        _temp = pandas.DataFrame()
        _temp['Time'] = pandas.Series([current_time] * len(stock_capital.index))
        _temp['Code'] = stock_capital.index
        _temp['Capital'] = stock_capital.values
        if len(self.selected_stocks) == 0:
            self.selected_stocks = _temp
        else:
            self.selected_stocks = self.selected_stocks.append(_temp, ignore_index=True)

    def back_up(self):
        """"""
        self.selected_stocks.to_csv(
            os.getcwd() + '/data/system_backup/selected_stocks.csv', sep=',')


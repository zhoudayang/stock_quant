#!/usr/bin/env python
# coding=utf-8

import os
import sys
import string
import ConfigParser


class Parser(object):
    """
    解析配置文件的类
    """
    def __init__(self):
        """"""
        self.config = ConfigParser.ConfigParser()
        self.config.read(os.getcwd()+'/config/config.conf')
        self.sections = None

    def get_sections(self):
        """
        得到配置文件的部分
        """
        self.sections = self.config.sections()
        return self.sections

    def get_options(self, sec_name):
        """
        得到每个部分的选项
        """
        return self.config.options(sec_name)

    def get_key_values(self, sec_name):
        """
        得到每个部分的内容
        """
        return self.config.items(sec_name)

    def get(self, sec_name, option):
        """
        得到每个部分的值
        """
        return self.config.get(sec_name, option)


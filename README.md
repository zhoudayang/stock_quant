Back Test 回测系统文档
===========
## 依赖包
- pandas
- numpy
- matplotplib
- orderedset
- MySQLdb

## 操作系统平台
- Windows(建议使用Anaconda)

    如果安装orderedset包出现需要VS C++ complier, 请到微软官网下载VS C++ complier

- Linux
- OS X

## 系统版本说明
- new2: 简单版本
- parallel_new2: 并行的简单版本（详细请转到parallel_new2版本查看）
- complex_new2: new2的复杂版本（复杂版本的说明请查看对应的说明文档）

## 系统框架设计

- [Account](#accout.py)

    程序运行时的主类。负责股票和股指期货的交易。

- [Strategy](#strategy.py)

    策略模块，定义起始时间，股票池，资金，基准

- [Performance](#performance.py)

    计算策略的指标，包括回撤率，夏普率，收益率等
    
- [Data](#data.py)

    数据模块，提供策略需要的数据，并且提供了对数据进行操作的行为，如：得到某个股票的价格，得到历史价格等

- [ResumeRecorder](#resume_recorder.py)

    数据备份模块，提供系统中断备份

- [utils](#matlab2py.py)

    系统使用的工具模块


## accout.py
---

最重要的模块，定义了Account这个主要的类

- Account类

    - 属性

            start = start
            end = end
            universe_temp = []
            all_universe = []
            self_defined = self_defined
            universe_code = universe_code
            universe = []#当天可以交易的股票池
            capital_base = capital_base
            freq = freq
            cash = capital_base
            IF_entryPrice = 0 #记录做空股指期货的价格
            data = {} #保存所有股票的每日历史数据，开盘价，最高价等信息，为字典，key为股票代码，值为该股票的历史数据，格式为DataFrame
            data_fuquan = {} # self.data的复权数据
            suspend_stock=[] #停牌的股票列表
            ST_stock = [] #特殊处理的股票列表
            dynamic_record = {} #股票的动态记录相关信息，每个交易日末添加新记录
            dynamic_record['trade_time']=[]#交易时间
            dynamic_record['cash']=[]#可用资金
            dynamic_record['capital']=[]#总价值
            dynamic_record['IF']=[] #做空股指期货的动态资产
            dynamic_record['blotter']=[]#下单记录
            dynamic_record['alpha_capital'] = [] #股票-做空股指期货的动态资产
            monthly_profit = {}  # 月收益
            static_profit = {} #静态收益，即是每次卖出股票才更新
            handlingFeeRate = 0.0003#手续率
            stampTaxRate = 0.001#印花税率
            valid_secpos = {} #为一个字典,key为股票代码,value是对应手头上持有的该股票的数量
            valid_secpos_price_today = {} #为一个字典,key为股票代码,value是今天的开盘价
            order_temp = [] #临时存储当天交易日的所有交易记录，即是许多order实例
            avgBuyprice = None #平均买入价格，在下面的iniData函数里初始化
            buyPosition = {} #为一个字典，key为股票代码，value是对应最近购买该股票过去的时间单位
            current_time = None #当前交易时间
            benchmark_code = benchmark_code  # 参考基准
            hold_IF = 0 #手头上持有的股指期货的手数
            dataIF = None #股指期货的历史数据
            market_capital = None
            lot_changed_rate = collections.OrderedDict() #记录每个月的换手率，其实这个指标应该是放到performance那里的
            real_deal_days = 0 #记录实际下单的天数
            stock_monthly_hold_profit = collections.OrderedDict() # 记录每个月股票持仓以及盈利
            IF_earning_rate = []
            stock_earning_rate = []
            position_changed_time = orderedset.OrderedSet() # 记录换仓的时间
            active_capital = 0 # 实际的可用资金
            active_universe = []
            temp_suspension_list = set()
            temp_down_limit_list = set()

    - 重要函数

            iniData: 加载数据，加载股票，股指期货的数据
            delete_exceptions: 删除异常股票，包括停牌，跌停，st
            order_to：下单函数
            sell_first_order_to: 换仓时有买有卖，先卖出，再买入
            isOk2order: 判断股票是否可以进行交易
            get_price/get_high_low/getDailyHistory: 获取历史数据（复权，基本）
            calculate_capital: 计算当前股票的动态资产
            calculata_IF: 计算股指期货的动态资产
            process_suspension_and_down_limit: 处理停牌和跌停股票
            get_latest_universe: 更新股票池
            calculate_turnover_rate: 计算换手率
            calculate_earning_rate: 计算收益率
            allocate_capital: 分配资金
            EntryIF： 做空股指期货
            EmptyIF:股指期货平仓

- OrderRecorder类

        记录下单

- get_alpha_rate函数

        计算alpha累计收益率

- get_earning_rate函数

        计算累计收益率

## strategy.py
---
策略模块

- 定义策略变量

        start = datetime.date(2016, 1, 29)  # 回测起始时间
        end  = datetime.date(2016, 3, 4)  # 回测结束时间
        benchmark = '000300.SH'	 # 策略参考标准

        capital_base = 1e6     # 起始资金
        short_capital = 1e6  # 做空资金

        freq = 'd'  # 时间单位

        universe_code = '000300.SH' # 除了沪深300，还有中证500，A股的等
        self_defined = False # 如果universe_code不是自定义的，就是False

        参考股票池：
            '''
            000002.SH: 上证A指
            000300.SH: 沪深300
            000010.SH: 上证180
            000016.SH: 上证50
            000903.SH: 中证100
            399330.SZ: 深证100
            399312.SH: 国证300
            399001.SZ: 深证成指
            A:                全部A股
            000905.SH: 中证500
            000906.SH: 中证800
            000852.SH: 中证1000
            '''

- 策略运行函数

   主要参考下面的函数，理解其流程

    ```python
        def handle_data(account, strategy_func=None):
            """
            """
            # 每天处理停牌和跌停的股票
            account.order_suspension_and_down_limit()
            if account.days_counts==1:
                return
            if account.is_my_deal_day(type='monthly', skip_time=datetime.date(1, 1, 1), run_time=datetime.date(1, 1, 1)):
                if self_defined:
                    latest_universe = universe_code
                else:
                    latest_universe = account.get_latest_universe()
                #处理历史上在沪深300中,但是这个月不在沪深300中的股票列表
                to_be_selled = set(account.all_universe).difference(set(latest_universe))
                for stock_id in to_be_selled:
                    #if account.isOk2order(stock_id):
                    today_open = account.get_price(stock_id, _open=True, _today=True)
                    #把所有在account.all_universe中但是不在latest_universe中的股票一一卖掉
                    account.order_to(stock_id, 0, today_open)
                # 处理股票
                process_stocks(account, latest_universe, strategy_func)
            # 止损, 每天都运行
            stop_loss(account)

        def process_stocks(account, latest_universe, strategy_func=None):
            """
            说明,这是一个简单的测试策略,用户可以根据需求自定义策略
            1. 对新的股票列表进行筛选,得到这个月第一个交易日的开盘价大于上
            个月最后一天的收盘价, 这样就得到了一个待下单的股票列表
            2. 对下单的股票列表的每个元素分别计算要下单的手数
            """
            # 每次换仓前，候选池要减去停牌，跌停以及st的股票
            account.delete_exceptions(suspension=True, down=True, up=True, st=True)
            # 选股策略
            reselected_stocks = strategy(account, strategy_func)
            # backup account.valid_secpos[account.valid_secpos>0]
            old_reselected_stocks = account.valid_secpos[account.valid_secpos>0]
            # 卖掉account.valid_secpos中没有出现在reselected_stocks中的股票,这里会记录停牌和跌停股票
            process_old_stocks(account, reselected_stocks)
            # 处理停牌和跌停的股票资产(如果历史记录的股票出现了停牌或跌停)
            active_capital = account.process_suspension_and_down_limit()
            # 得到股票的资本分配
            stock_capital = account.allocate_capital(active_capital, reselected_stocks, strategy=1)
            # 调用order_to
            deal_new_stock(account, stock_capital)
            # 记录换手率，并且买卖
            account.calculate_turnover_rate(stock_capital, old_reselected_stocks)
    ```
    需要注意的是
    - `handle_data` 根据换仓的频率（每日，每周，每月，每季度等）来选股
    - `reselected_stocks` 是每次选股之后得到的股票列表，用户从`handle_data`传入自定义的策略函数

## performance.py
---
计算指标
- Performance

    计算回测率，换手率，收益率等

## data.py
---
数据类
- StockData类

    获取股票数据（包括基准数据，股票池，基本价格数据，复权数据等）

- IndexFuturesData类

    获取股指期货数据

- IndustryData类

    申万，中信，证监会

- Quota类

    获取相应的指标

- DBConnection类

    数据库连接类，初始化数据库的连接

## resume_recorder.py
---
系统运行备份类
- ResumeRecorder类

    在Account初始化实例时初始化，然后记录程序运行的数据和状态

## config_parser.py
---
配置文件解析
- Parser类

    解析配置文件

## matlab2py.py
---
matlab格式的数据转换成python格式的数据

## 运行主函数
---
运行`my_test.py`, 从运行实例中理解系统的运行流程

        $ python my_test.py
        正在加载数据，请等待... read A_stock_day:  3.04443192482
        time costs 1.76007699966
        read A_stock_day_fuquan:  2.5732011795
        time costs: 1.85459089279
        加载数据成功
        >>>  2016-02-01 00:00:00  handle data
        -------------------------------------deal new stock start-------------------------------------
        --------------------------------------deal new stock end--------------------------------------
        >>>  2016-03-01 00:00:00  handle data
        INFO: stock--该天不能买卖，原因:停牌	id:600170.SH	num:0 current_num:3100	price:4.92999982834
        INFO: stock--该天不能买卖，原因:停牌	id:000651.SZ	num:0 current_num:700	price:19.2199993134
        INFO: stock--该天不能买卖，原因:停牌	id:600000.SH	num:0 current_num:800	price:18.4500007629
        -------------------------------------deal new stock start-------------------------------------
        --------------------------------------deal new stock end--------------------------------------
        ------------------------2016-03-02 00:00:00 deal suspension stock start------------------------
        INFO: stock--该天不能买卖，原因:停牌	id:600170.SH	num:0 current_num:3100	price:4.92999982834
        INFO: stock--该天不能买卖，原因:停牌	id:600000.SH	num:0 current_num:800	price:18.4500007629
        INFO: stock--该天不能买卖，原因:停牌	id:000651.SZ	num:0 current_num:700	price:19.2199993134
        ------------------------2016-03-02 00:00:00 deal suspension stock end------------------------
        ------------------------2016-03-03 00:00:00 deal suspension stock start------------------------
        INFO: stock--该天不能买卖，原因:停牌	id:600170.SH	num:0 current_num:3100	price:4.92999982834
        INFO: stock--该天不能买卖，原因:停牌	id:600000.SH	num:0 current_num:800	price:18.4500007629
        INFO: stock--该天不能买卖，原因:停牌	id:000651.SZ	num:0 current_num:700	price:19.2199993134
        ------------------------2016-03-03 00:00:00 deal suspension stock end------------------------
        换仓日: 2016-02-01 00:00:00
        当天alpha收益率为0
        当天alpha累计收益率为1
        当天alpha收益率为0.00543767437673
        当天alpha累计收益率为1.00543767438
        换仓日: 2016-02-03 00:00:00
        当天alpha收益率为0.00198977861336
        当天alpha累计收益率为1.00742745299
        当天alpha收益率为0.00123408201013
        当天alpha累计收益率为1.00867070109
        当天alpha收益率为0.0104760177841
        当天alpha累计收益率为1.019224529
        当天alpha收益率为0.00162861621883
        当天alpha累计收益率为1.02086524169
        当天alpha收益率为0.00342459220103
        当天alpha累计收益率为1.02431526989
        当天alpha收益率为0.00344796770041
        当天alpha累计收益率为1.02778884721
        当天alpha收益率为-0.00338110721653
        当天alpha累计收益率为1.02438262697
        当天alpha收益率为0.000471060377929
        当天alpha累计收益率为1.02485718613
        当天alpha收益率为0.0218176872307
        当天alpha累计收益率为1.04683692321
        当天alpha收益率为-0.00135651003124
        当天alpha累计收益率为1.04547033776
        当天alpha收益率为0.00340514602846
        当天alpha累计收益率为1.04890077535
        换仓日: 2016-02-25 00:00:00
        当天alpha收益率为-0.00546402863776
        当天alpha累计收益率为1.0433961629
        当天alpha收益率为0.00205765960292
        当天alpha累计收益率为1.04554311703
        换仓日: 2016-02-29 00:00:00
        当天alpha收益率为-0.000900980818041
        当天alpha累计收益率为1.04460303711
        换仓日: 2016-03-01 00:00:00
        当天alpha收益率为-0.00752688026386
        当天alpha累计收益率为1.03674043512
        当天alpha收益率为-0.00292110086613
        当天alpha累计收益率为1.03371201174
        当天alpha收益率为0.00249018894784
        当天alpha累计收益率为1.03629369131
        年份 2016 alpha 0.0362936913123
        年份 2016 stock 0.0812744617503

程序会把中间结果存放到`data/output_data`目录下， 最后程序会显示一张图，显示了股票，股指期货和alpha收益率的折线图，如下图所示

## 账户说明

值得注意的是，股票和股指期货的账户是单独分开的两个不同的账户，二者资金是独立的，因此叫做简单版本。而复杂版本的股票和股指期货使用的是共同账户

## fix one bug

当开盘价格为空时，原系统会因为数据缺失导致计算的cash为nan,已经修复这个bug.

## dde选股策略

选取dde大于0，流通市值从小到大排列的股票


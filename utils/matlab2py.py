import datetime

def matlab2datetime(matlab_datenum):
    """
    >>> import back_test.utils.matlab2py
    >>> back_test.utils.matlab2py.matlab2datetime(734275)
    datetime.datetime(2010, 5, 17, 0, 0)
    """
    day = datetime.datetime.fromordinal(int(matlab_datenum))
    dayfrac = datetime.timedelta(days=matlab_datenum%1) - datetime.timedelta(days = 366)
    return day + dayfrac

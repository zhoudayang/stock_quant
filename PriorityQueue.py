# __author__ = 'fit'
# -*- coding: utf-8 -*-

from heapq import heappush, heappop
# python优先队列,按照值的大小进行排序,值越小,越排在前面
class PriorityQueue:
    def __init__(self):
        self._queue = []

    def put(self, item, priority):
        heappush(self._queue, (priority, item))

    def get(self):
        return heappop(self._queue)[-1]
    def size(self):
        return len(self._queue)

    # 获取前面n个元素组成的list,返回的list大小不超过队列的长度
    def get_list(self,n):
        result =[]
        size =self.size()
        if(size>=n):
            for i in range(0,n):
                result.append(self.get())
        else:
            for i in range(0,size):
                result.append(self.get())
        return result




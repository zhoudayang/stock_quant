Back Test回测系统开发者文档
---

# 版本说明

* 目前版本：　new2
* 版本类型：　简单版本
* 是否支持并行：　否

# 系统文件结构

    $ tree back_test 
    back_test
    ├── account.py
    ├── config
    │   ├── config.conf
    │   ├── config_parser.py
    │   └── __init__.py
    ├── data
    │   ├── __init__.py
    │   ├── output_data
    │   │   └── README.md
    │   ├── system_backup
    │   │   └── README.md
    │   └── system_data
    │       ├── data.py
    │       ├── deliveryInfo.mat
    │       └── __init__.py
    ├── doc
    │   └── back_test.md
    ├── __init__.py
    ├── my_test.py
    ├── performance.py
    ├── resume_recorder.py
    ├── run.bash
    ├── strategy.py
    └── utils
        ├── __init__.py
        └── matlab2py.py
   
    7 directories, 21 files

# coding=utf-8
__author__ = 'FIT Capital'
import math
import datetime
import functools
import collections

import orderedset
import numpy as np
import pandas as pd

import resume_recorder
import data.system_data.data

import copy

class OrderRecorder():
    """
    记录下单记录的类，每个实例表示一个下单记录
    """

    def __init__(self, time, code, num, price):
        """"""
        self.order_time = time
        self.code = code
        self.order_num = num
        self.price = price
        self.profit = 0


class Account():
    """
    这是整个系统中最关键的类，它的变量存储着基本历史数据和交易信息，也有许多数据接口和下单函数
    """

    def __init__(self, start, end, freq, universe_code, capital_base, short_capital, benchmark_code, self_defined):
        """
        初始化函数，可参考下面的调用用法
        :param start: 下面几个参数都是策略my_strategy里面的关键参数
        :param end:
        :param freq:
        :param universe_code:
        :param capital_base:
        :param short_capital:
        :param benchmark_code:
        :param self_defined:
        :return:
        """
        self.start = start
        self.end = end
        self.universe_temp = []
        self.all_universe = []
        self.self_defined = self_defined
        self.universe_code = universe_code
        self.universe = []  # 当天可以交易的股票池
        self.capital_base = capital_base
        self.short_capital = short_capital
        self.freq = freq
        # 初始化cash为capital_base
        self.cash = capital_base
        self.short_cash = short_capital
        # self.commisson=commisson
        self.futures_entryPrice = 0  # 记录做空股指期货的价格
        self.data = {}  # 保存所有股票的每日历史数据，开盘价，最高价等信息，为字典，key为股票代码，值为该股票的历史数据，格式为DataFrame
        self.data_fuquan = {}  # self.data的复权数据
        self.suspend_stock = []  # 停牌的股票列表
        self.ST_stock = []  # 特殊处理的股票列表
        self.dynamic_record = {}  # 股票的动态记录相关信息，每个交易日末添加新记录
        self.dynamic_record['trade_time'] = []  # 交易时间
        self.dynamic_record['cash'] = []  # 可用资金
        self.dynamic_record['capital'] = []  # 总价值
        self.dynamic_record['futures'] = []  # 做空股指期货的动态资产
        self.dynamic_record['blotter'] = []  # 下单记录
        self.dynamic_record['alpha_capital'] = []  # 股票-做空股指期货的动态资产
        self.DR = pd.DataFrame()  # 动态记录
        self.monthly_profit = {}  # 月收益
        self.static_profit = {}  # 静态收益，即是每次卖出股票才更新
        self.handlingFeeRate = 0.0003  # 手续率
        self.stampTaxRate = 0.001  # 印花税率
        self.valid_secpos = {}  # 为一个字典,key为股票代码,value是对应手头上持有的该股票的数量
        self.valid_secpos_price_today = {}  # 为一个字典,key为股票代码,value是今天的开盘价
        self.order_temp = []  # 临时存储当天交易日的所有交易记录，即是许多order实例
        self.avgBuyprice = None  # 平均买入价格，在下面的iniData函数里初始化
        self.buyPosition = {}  # 为一个字典，key为股票代码，value是对应最近购买该股票过去的时间单位
        self.current_time = None  # 当前交易时间
        self.days = 0
        self.benchmark = None
        # self.benchmark_code = '000300.sh'
        self.benchmark_code = benchmark_code  # 参考基准
        self.max_value = []
        self.total_fees = 0
        self.hold_futures = 0  # 手头上持有的股指期货的手数
        self.data_futures = None  # 股指期货的历史数据
        # self.dataIC = None
        # self.dataIH = None
        self.dataAStockBasic = None
        self.AStockIndusCitiClass = None
        self.AstockIndusCiti = None
        self.AStockIndusCSRCClass = None
        self.AStockIndusCSRC = None
        self.AStockIndusSWClass = None
        self.AStockIndusSW = None
        self.market_capital = None
        self.is_short = False
        self.lot_changed_rate = collections.OrderedDict()  # 记录每个月的换手率，其实这个指标应该是放到performance那里的
        self.real_deal_days = 0  # 记录实际下单的天数
        self.stock_daily_hold_profit = collections.OrderedDict()  # 记录每日股票持仓以及盈利
        self.futures_earning_rate = []
        self.stock_earning_rate = []
        self.position_changed_time = orderedset.OrderedSet()  # 记录换仓的时间
        self.active_capital = 0
        self.active_universe = []
        self.temp_suspension_list = set()
        self.temp_down_limit_list = set()
        self.temp_nan_list = set()
        self.details_of_deal = None  # 记录股票的交易细节，包括时间，股票id，状态，交易的几手，当前手数，当前价格，当前均价
        self.details_of_calculate_capital = None  # 记录每次计算动态收益的细节，包括时间，股票id，盈亏，当前时间的cash
        self.stock_deal_details = []  # 股票的交易细节
        self.calculate_capital_details = []  # 计算动态资产细节
        self.backup_data = None
        self.resume_system = True
        self.sys_recorder = None

    def iniData(self):
        """
        初始化一些相关数据
        :return:
        """
        print "正在加载数据，请等待...",
        # init backup data
        try:
            self.backup_data = pd.read_csv('./data/system_backup/selected_stocks.csv', index_col=0)
            self.resume_times = self.backup_data['Time']
            print "检测到系统上次运行发生了中断，系统正在为你加载历史数据，以便快速恢复现场."
        except Exception, e:
            self.resume_system = False
        self.sys_recorder = resume_recorder.ResumeRecorder()
        stock_data = data.system_data.data.StockData(self.freq,
                                                     self.benchmark_code,
                                                     self.start,
                                                     self.end,
                                                     self.self_defined,
                                                     self.universe_code
                                                     )
        index_future_data = data.system_data.data.IndexFuturesData(self.start, self.end, self.benchmark_code)
        # industry_data  = data.system_data.data.IndustryData(self.start, self.end)
        # init stock data
        self.timeList = stock_data.get_time_list()
        self.days = stock_data.days
        self.all_universe, self.universe_temp = stock_data.get_all_universe()
        self.benchmark = stock_data.get_benchmark_data()
        self.suspend_stock = stock_data.get_suspened_stocks()
        self.ST_stock = stock_data.get_ST_stocks()
        self.data = stock_data.get_data(self.all_universe)
        self.data_fuquan = stock_data.get_data_fuquan(self.all_universe)
        # self.market_capital = self.stock_data.get_market_capital()
        # init index future data
        self.data_futures = index_future_data.get_futures_data()
        # self.dataIH = self.index_future_data.get_IH_data()
        # self.dataIC = self.index_future_data.get_IC_data()
        # init industry data
        # self.AStockIndusCitiClass, self.AstockIndusCiti = \
        #        self.industry_data.get_citic()
        # self.AStockIndusCSRCClass, self.AStockIndusCSRC = \
        #        self.industry_data.get_csrc()
        # self.AStockIndusSWClass, self.AStockIndusSW = \
        #        self.industry_data.get_sw()
        del stock_data
        del index_future_data
        # del industry_data
        #####初始化相关变量#####
        self.valid_secpos = pd.Series(0, self.all_universe)
        self.valid_secpos_price = pd.Series(0.0, self.all_universe)
        self.valid_secpos_price_today = pd.Series(0.0, self.all_universe)
        self.avgBuyprice = pd.Series(0.0, self.all_universe)
        self.buyPosition = pd.Series(0, self.all_universe)
        for i in self.all_universe:
            self.static_profit[i] = 0
        print "加载数据成功"

    def delete_exceptions(self, suspension=True, down=True, up=True, st=True):
        """
        让用户选择需要删除的异常股票
        按照用户换仓的频率
        """
        self.temp_suspension_list.clear()
        self.temp_down_limit_list.clear()
        self.active_universe = self.get_latest_universe()
        print "len:%s" % len(self.active_universe)
        if suspension == True:
            self.delete_suspension()
        if up == True:
            self.delete_up_limit(type='up')
        if down == True:
            self.delete_down_limit(type='down')
        if st == True:
            self.delete_st_stocks()

    def delete_suspension(self):
        """
        每次换仓删除停牌股票
        """
        suspension_list = self.suspend_stock[self.suspend_stock['DATE(date)'] == self.current_time][
            'code'].values.tolist()
        # get_latest_universe() 得到当天沪深300的股票
        self.active_universe = list(set(self.active_universe) - set(suspension_list))

    def delete_up_or_down(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kw):
            _temp_list = []
            _st_stocks = self.ST_stock[self.ST_stock['DATE(date)'] == self.current_time]['wind_code'].values
            for stock in self.active_universe:
                yesterday = self.getDailyHistory(stock, 1)['close'].values
                today_open = self.get_price(stock, _open=True, _today=True)
                if not today_open:  # 2015-8-28 策略调用数据问题修改
                    _temp_list.append(stock)
                elif len(yesterday) == 0:
                    print "当前为第一天无法取得前一天的数据"
                    continue
                elif kw['type'] == 'up':
                    if stock in _st_stocks:
                        if today_open == round(yesterday[-1] * 1.05, 2):  # 涨停
                            _temp_list.append(stock)
                    if today_open == round(yesterday[-1] * 1.1, 2):  # 涨停
                        _temp_list.append(stock)
                elif kw['type'] == 'down':
                    if stock in _st_stocks:
                        if today_open == round(yesterday[-1] * 0.95, 2):  # 跌停
                            _temp_list.append(stock)
                    if today_open == round(yesterday[-1] * 0.9, 2):  # 跌停
                        _temp_list.append(stock)
            self.active_universe = list(set(self.active_universe) - set(_temp_list))
            return func(self, *args, **kw)

        return wrapper

    @delete_up_or_down
    def delete_down_limit(self, *args, **kw):
        """
        每次换仓删除跌停股票
        """
        pass

    @delete_up_or_down
    def delete_up_limit(self, *args, **kw):
        """
        处理涨停股票
        """
        pass

    def delete_st_stocks(self):
        """
        每次换仓删除st股票
        """
        st_list = self.ST_stock[self.ST_stock['DATE(date)'] == self.current_time]['wind_code'].values
        self.active_universe = list(set(self.active_universe) - set(st_list))

    def isOk2order(self, code, buy_or_sale, price):  # 判断该股票是否可以交易
        """
        """
        code = code.upper()
        if pd.isnull(price):
            print "price is nan!"
            self.temp_nan_list.add(code)
            return False, "数据缺失！"
        stock_list = self.suspend_stock[self.suspend_stock['DATE(date)'] == self.current_time]['code'].values  # 判断停牌
        if code in stock_list:
            # 记录停牌股票
            self.temp_suspension_list.add(code)
            return False, '停牌'
        elif self.is_up_limit(code=code, type='up'):  # 判断涨停,可以卖,不能买
            if buy_or_sale > 0:  # 买
                return False, '涨停'
            else:
                return True, '正常交易'
        elif self.is_down_limit(code=code, type='down'):  # 判断跌停,不能卖，可以买
            if buy_or_sale > 0:  # 买
                return True, '正常交易'
            else:
                # 记录跌停股票
                self.temp_down_limit_list.add(code)
                return False, '跌停'
        else:
            return True, '正常交易'

    def is_up_or_down(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kw):
            yesterday = self.getDailyHistory(kw['code'].upper(), 1)['close'].values
            today_open = self.get_price(kw['code'], _open=True, _today=True)
            if not today_open:  # 2015-8-28 策略调用数据问题修改
                # print "没open数据,但是是在涨跌停中判断"
                return True
            if len(yesterday) == 0:
                # print "当前为第一天无法取得前一天的数据"
                return False
            stock_list = self.ST_stock[self.ST_stock['DATE(date)'] == self.current_time]['wind_code'].values
            if kw['type'] == 'up':
                if kw['code'] in stock_list:
                    if today_open == round(yesterday[-1] * 1.05, 2):  # 涨停
                        return True
                    else:
                        return False
                else:
                    if today_open == round(yesterday[-1] * 1.1, 2):  # 涨停
                        return True
                    else:
                        return False
            elif kw['type'] == 'down':
                if kw['code'] in stock_list:
                    if today_open == round(yesterday[-1] * 0.95, 2):  # 跌停
                        return True
                    else:
                        return False
                else:
                    if today_open == round(yesterday[-1] * 0.9, 2):  # 跌停
                        return True
                    else:
                        return False
            return func(*args, **kw)

        return wrapper

    @is_up_or_down
    def is_up_limit(self, *args, **kw):
        """
        判断涨停
        :param code:
        :return Fool:
        """
        pass

    @is_up_or_down
    def is_down_limit(self, *args, **kw):
        """
        判断跌停
        :param code:
        :return Fool:
        """
        pass

    def get_price(self, code, _open=True, _today=True, is_fuquan=False):
        """
        得到该股票的开盘价/收盘价，float类型
        """
        _code = code.upper()
        _temp = self.data_fuquan[_code] if is_fuquan else self.data[_code]
        try:
            return _temp[_temp['date_time'] <= self.current_time if _today else \
                self.current_time - datetime.timedelta(days=1)].tail(1)['open' if _open else 'close'].values[-1]
        except IndexError, e:
            # 还未上市或者数据库里在这时间段没数据
            return None

    def get_high_low(self, code, _high=True, _today=True, is_fuquan=False):
        """
        得到最高价或者最低价
        """
        _code = code.upper()
        _temp = self.data_fuquan[_code] if is_fuquan else self.data[_code]
        try:
            return _temp[_temp['date_time'] <= self.current_time if _today else \
                self.current_time - datetime.timedelta(days=1)].tail(1)['high' if _high else 'low'].values[-1]
        except IndexError, e:
            # 还未上市或者数据库里在这时间段没数据
            return None

    def getDailyHistory(self, code, length, is_fuquan=False):
        """
        input:股票代码，str类型；天数,int
        return:该股票在过去length天的所有历史数据(is_basic是True返回基本数据，否则返回复权数据)，DataFrame类型
        """
        code = code.upper()
        temp = self.data_fuquan[code] if is_fuquan else self.data[code]
        return temp[temp['date_time'] < self.current_time].tail(length)

    def getFuturesData(self, type, t):
        """"""
        return self.data_futures[self.data_futures['Date'] <= t].tail(1)[type].values[-1]

    def commission(self, firstCode, price, status=1, number=0):
        """
        计算交易费用
        :param firstCode: 股票代码的第一个数字
        :param price: 当前价格
        :param status: 买卖状态
        :param number: 买卖数量
        :return:买卖费用
        """
        fee = 0
        if firstCode == 6:  # 上海股票
            scripFee = number / 1000 + 1  # 过户费
            handlingFee = price * number * self.handlingFeeRate  # 手续费
            if handlingFee < 5:
                handlingFee = 5
            fee = handlingFee + scripFee
            if status == -1:
                stampTax = price * number * self.stampTaxRate
                fee += stampTax

        elif firstCode == 0 or firstCode == 3:  # 深圳股票
            handlingFee = price * number * self.handlingFeeRate  # 手续费
            fee = handlingFee
            if status == -1:
                stampTax = price * number * self.stampTaxRate
                fee += stampTax

        elif firstCode == 5:  # 沪基金
            scripFee = number / 1000 + 1  # 过户费
            handlingFee = price * number * self.handlingFeeRate  # 手续费
            if handlingFee < 5:
                handlingFee = 5
            fee = handlingFee + scripFee

        elif firstCode == 4:  # 深基金
            handlingFee = price * number * self.handlingFeeRate  # 手续费
            fee = handlingFee

        return fee

    def calculate_capital(self, time):
        """
        调用时计算给定时间的动态资产，就是将所有股票持有手数乘以当天的收盘价之和
        :param time: 回测的时间
        :return:动态资产
        """
        for i in self.all_universe:
            if self.valid_secpos[i] == 0:
                self.valid_secpos_price[i] = 0
            else:
                _detail = []
                temp = self.data[i]
                if len(temp[temp['date_time'] == self.current_time]['close'].values) != 0:
                    self.valid_secpos_price[i] = temp[temp['date_time'] == self.current_time]['close'].values[0]
                    _detail.append(str(self.current_time))
                    _detail.append(str(i))
                    _detail.append(str(self.valid_secpos[i]))
                    _detail.append(str(self.valid_secpos_price[i]))
                    _detail.append(str(self.avgBuyprice[i]))
                    _detail.append(str(self.valid_secpos_price[i] - self.avgBuyprice[i]))
                    _detail.append(str(self.cash))
                    self.calculate_capital_details.append(_detail)
        temp = self.valid_secpos_price * self.valid_secpos
        return temp.sum() + self.cash

    def calculate_today_capital(self):
        """
        利用今天开盘价计算总动态资产
        """
        for i in self.all_universe:
            if self.valid_secpos[i] == 0:
                self.valid_secpos_price_today[i] = 0
            else:
                temp = self.data[i]
                if len(temp[temp['date_time'] == self.current_time]['open'].values) != 0:
                    self.valid_secpos_price_today[i] = temp[temp['date_time'] == self.current_time]['open'].values[0]
        temp = self.valid_secpos_price_today * self.valid_secpos
        # 此处的self.cash为nan
        return temp.sum() + self.cash

    def calculate_futures(self, time):
        """
        计算股指期货的资产
        :param time:
        :return:
        """
        try:
            if self.hold_futures == 0:
                futures_temp = 0
            else:
                futures_temp = (self.futures_entryPrice + self.futures_entryPrice * 0.15 -
                                self.data_futures[self.data_futures['Date'] == self.current_time]['Close'].values[
                                    0]) * 300 * self.hold_futures
        except IndexError, e:
            print self.current_time, "futures数据缺失！"
            exit()
        # self.dynamic_record['IF'].append(IF_temp + self.short_cash)
        return futures_temp + self.short_cash

    def EntryFutures(self, money):
        """
        做空股指期货
        :param money:用来做空的钱
        :return:
        """
        self.is_short = True
        Open = self.data_futures[self.data_futures['Date'] == self.current_time]['Open'].values[0]
        self.futures_entryPrice = Open
        lot = int(money / (Open * 300.0))
        fee = Open * 300 * 6 / 100000.0
        self.short_cash -= lot * Open * 300.0 * 0.15 + fee
        # self.cash-=lot*Open*300.0*0.15-fee
        self.hold_futures += lot

    def EmptyFutures(self, lot):
        """
        平几手股指期货
        :param lot:几手
        :return:
        """
        self.is_short = True
        Open = self.data_futures[self.data_futures['Date'] == self.current_time]['Open'].values[0]
        fee = Open * 300 * 6 / 100000.0
        self.short_cash += lot * (self.futures_entryPrice - Open + self.futures_entryPrice * 0.15) * 300.0 - fee
        # self.cash+=lot*Open*300.0*0.15+fee
        self.hold_futures -= lot

    def order(self, code, num, price):
        """
        下单函数,买多少手
        :param code: 股票代码
        :param num: 手数
        :return:
        """
        # todayOpen=self.getTodayOpen(code)
        old_num = self.valid_secpos[code]
        # print old_num*self.avgBuyprice[code],'  ',price*num,'  ',old_num+num
        new_order = OrderRecorder(self.current_time, code, num, price)
        self.order_temp.append(new_order)
        # .append(new_order)
        self.valid_secpos[code] += num
        if num > 0:
            status = 1
        else:
            status = -1
        fee = self.commission(int(code[0]), price, status, abs(num))
        if num < 0:
            self.static_profit[code] += abs(num) * (price - self.avgBuyprice[code])  # 计算每只股票的静态收益
            # print self.avgBuyprice[code],price,num
        # 计算股票持仓的平均价格
        if old_num + num == 0:
            self.avgBuyprice[code] = 0  # 空仓清零
        elif num > 0:
            self.avgBuyprice[code] = (old_num * self.avgBuyprice[code] + price * num) / float(old_num + num)  # 求平均
            # print self.avgBuyprice[code],'c'
        # here price can be nan. which lead self.cash to nan
        self.cash = self.cash - num * price - fee
        self.buyPosition[code] = self.days_counts

    def order_to(self, code, num, price):
        """
        下单函数，买或者卖到num手
        :param code:
        :param num:
        :return:
        """
        code = code.upper()
        current_num = self.valid_secpos[code]
        if num == current_num or num < 0:
            return
        can_deal, deal_status = self.isOk2order(code, num - current_num, price)  # 判断能不能买卖(涨停，跌停，停牌)
        if can_deal:
            self.order(code, num - current_num, price)
            self.position_changed_time.add(self.current_time)  # 发生了换仓
        else:
            print "INFO: stock--该天不能买卖，原因:%s\tid:%s\tnum:%s current_num:%s\tprice:%s" % (
                str(deal_status), str(code), num, current_num, price)
        if can_deal and (num - current_num) > 0:
            deal_status = '买'
        elif can_deal and (num - current_num) <= 0:
            deal_status = '卖'
        _temp = []
        _temp.append(str(self.current_time))
        _temp.append(str(code))
        _temp.append(str(deal_status))
        _temp.append(str(num))
        _temp.append(str(current_num))
        _temp.append(str(price))
        _temp.append(str(self.avgBuyprice[code]))
        self.stock_deal_details.append(_temp)

    # 买入股票
    def sell_first_order_to(self, stock_capital, priority_time=datetime.date(1, 1, 1)):
        """
        优先卖出
        """
        _temp = {}
        for _id, _cap in stock_capital.iteritems():
            price = self.get_price(_id, _open=True, _today=True)
            price = float(price)
            if pd.isnull(price):
                print "缺少开盘价数据,买入失败！"
                continue
            try:
                _temp[_id] = int(math.ceil(_cap / price)) / 100 * 100 - \
                             self.valid_secpos[_id]
            except:
                print "get nan value error!"
                print _id,_cap
                print  "end"
        # _temp = \
        #     {_id: int(math.ceil(_cap/float(self.get_price(_id, _open=True, _today=True)))) / 100 * 100 -\
        #          self.valid_secpos[_id]
        #              for _id, _cap in stock_capital.iteritems()}
        _count = 0
        # 按照买入的数量进行排序
        for _stock_id, _ in sorted(_temp.iteritems(), key=lambda _v: _v[1], reverse=False):
            if self.current_time.date() == priority_time:
                _today_open = self.getYesterdayClose(_stock_id)
            else:
                _today_open = self.get_price(_stock_id, _open=True, _today=True)
            if pd.isnull(_today_open):
                print "_today_open is null! drop it!"
                continue
            _num = int(math.ceil(stock_capital[_stock_id] / float(_today_open))) / 100 * 100
            if self.not_enough_cash(_stock_id, _num, _today_open):
                print '\033[1;44m%s \033[1;m' % ' \
                    INFO: dealed %d stocks in reselected_stocks, total %d, stop ' \
                                                'deal at %s, now break' % (_count, len(stock_capital.values), _stock_id)
                break
            self.order_to(_stock_id, _num, _today_open)

    def is_my_deal_day(self, type='monthly', skip_time=datetime.date(1, 1, 1), run_time=datetime.date(1, 1, 1)):
        """
        判断今天是否是换仓日
        """
        if type == 'monthly':
            if self.current_time.month != self.last_time.month or \
                                            self.current_time.year == run_time.year and \
                                            self.current_time.month == run_time.month and \
                                    self.current_time.day == run_time.day:
                if self.current_time.year == skip_time.year and \
                                self.current_time.month == skip_time.month and \
                                self.current_time.day == skip_time.day:
                    return False
                else:
                    self.real_deal_days += 1
                    print '\033[1;46m%s \033[1;m' % '>>>', '\033[1;46m%s \033[1;m' % self.current_time, '\033[1;46m%s \033[1;m' % 'handle data'
                    return True
        elif type == 'daily':
            pass
        elif type == 'other':
            pass
        return False

    def order_suspension_and_down_limit(self):
        """
        每天处理停牌和跌停的股票
        """
        # Every day process suspension and down limit stock
        if len(self.temp_suspension_list) != 0:
            print '------------------------%s deal suspension stock start------------------------' % self.current_time
            for stock_id in self.temp_suspension_list:  # suspension_list:
                today_open = self.get_price(stock_id, _open=True, _today=True)
                if today_open is None:
                    print "处理停牌股票%s时，开盘价缺失。" % stock_id
                    continue
                self.order_to(stock_id, 0, today_open)
            print '------------------------%s deal suspension stock end------------------------' % self.current_time
        if len(self.temp_down_limit_list) != 0:  # down limit list
            print '------------------------%s deal down limit stock start------------------------' % self.current_time
            for stock_id in self.temp_down_limit_list:
                today_open = self.get_price(stock_id, _open=True, _today=True)
                if today_open is None:
                    print "处理跌停股票%s时，开盘价缺失。" % stock_id
                    continue
                self.order_to(stock_id, 0, today_open)
            print '------------------------%s deal down limit stock end------------------------' % self.current_time
        if len(self.temp_nan_list)!=0:
            print '------------------------%s deal nan stock start------------------------' % self.current_time
            temp_nan_list = copy.deepcopy(self.temp_nan_list)
            self.temp_nan_list.clear()
            for stock_id in temp_nan_list:
                today_open = self.get_price(stock_id, _open=True, _today=True)
                self.order_to(stock_id, 0, today_open)
            print '------------------------%s deal nan stock end------------------------' % self.current_time

    def process_suspension_and_down_limit(self):
        """
        每个月的候选池是没有停牌和跌停股票
        选股之后股票可能会是停牌或者是跌停，这部分资金需要减去
        """
        self.active_capital = 0  # 每次处理前置零
        # 处理停牌股票
        temp_suspension_capital = 0
        for index, stock_id in enumerate(self.temp_suspension_list):
            temp_suspension_capital += self.valid_secpos[stock_id] * self.avgBuyprice[stock_id]
        # 处理跌停股票
        temp_down_limit_capital = 0
        for index, stock_id in enumerate(self.temp_down_limit_list):
            temp_down_limit_capital += self.valid_secpos[stock_id] * self.avgBuyprice[stock_id]
        print ("calculate capital begin")
        print(self.current_time)
        # 　涨停股不用减
        # 此处为ｎａｎ
        self.active_capital = self.calculate_today_capital()
        print (self.active_capital)
        print (temp_suspension_capital)
        print (temp_down_limit_capital)
        print ("calculate capital end")
        self.active_capital -= (temp_down_limit_capital + temp_suspension_capital)
        # self.active_capital = self.calculate_today_capital() - temp_suspension_capital - temp_down_limit_capital
        return self.active_capital

    def isBeginOfMonth(self, t):
        """
        判断是否未月的第一天
        :param t,类型datetime。date:
        :return:
        """
        temp = self.timeList[self.timeList < t].tail(1).values[0]
        # print temp.values[0]
        if temp.month != t.month:
            return True
        else:
            return False

    def getBeginOfMonth(self, y, m):
        """
        得到某年某月的第一个交易日
        :param y: 年，int
        :param m: 月，int
        :return:datetime
        """
        t = datetime.date(y, m, 1)
        return self.timeList[self.timeList >= t].head(1).values[0]

    def get_latest_universe(self):
        """
        更新股票池
        :return: 最新的股票代码对应的股票池
        """
        if self.universe_code == 'A':
            # return self.all_universe
            latest_universe = self.universe_temp[(self.universe_temp['date'] == self.current_time)
                                                 & (self.universe_temp[
                                                        'group_code'] == '%s' % self.universe_code.upper())].tail(1)[
                'content'].values[0].split(',')
        else:
            latest_universe = self.universe_temp[(self.universe_temp['date'] <= self.current_time)
                                                 & (self.universe_temp[
                                                        'group_code'] == '%s' % self.universe_code.upper())].tail(1)[
                'content'].values[0].split(',')
        return latest_universe

    def calculate_monthly_profit(self):
        """
        计算月收益
        :return:
        """
        if self.current_time is None or self.last_time is None:
            return
        if self.current_time.month != self.last_time.month:
            y = self.last_time.year
            m = self.last_time.month
            last_begin = self.getBeginOfMonth(y, m)
            last_end = self.last_time
            last_all_profit = self.DR[last_begin:last_end]['capital']
            last = self.last_time.strftime('%Y-%m')
            self.monthly_profit[last] = last_all_profit[-1] - last_all_profit[0]
            self.monthly_profit['last_month'] = last_all_profit[-1] - last_all_profit[0]

    def calculate_stock_daily_hold_profit(self):
        """
        记录每日的股票的持股，收益情况
        """
        if self.current_time is None or self.last_time is None:
            return
        profit = []
        time = [self.current_time] * len(self.valid_secpos[self.valid_secpos > 0])
        for index in self.valid_secpos[self.valid_secpos > 0].index:
            # 买入手续费
            # buy_fee = self.commission(int(index[0]),self.avgBuyprice[index],
            #                           status=1, number=abs(self.valid_secpos[index]))
            profit.append(self.valid_secpos[index] * (self.valid_secpos_price[index] - self.avgBuyprice[index]))
        temp = np.array([time, self.valid_secpos[self.valid_secpos > 0].index.tolist(),
                         self.valid_secpos[self.valid_secpos > 0].tolist(), profit])
        column_name = [u'time ', u'index ', u'hold_lot ', u'profit ']
        self.stock_daily_hold_profit[self.current_time] = pd.DataFrame(temp.T, columns=column_name)

    def not_enough_cash(self, stock_id, lot_te_be_dealed, open_price):
        """
        判断当前的cash可否支持这次股票交易
        """
        current_num = self.valid_secpos[stock_id]
        num = lot_te_be_dealed - current_num
        if num == 0:
            return False
        if num > 0:
            status = 1
        else:
            status = -1
        fee = self.commission(int(stock_id[0]), open_price, status, abs(num))
        if num > 0 and self.cash > (num * open_price + fee):  # 买
            return False
        elif num < 0:  # 卖
            return False
        return True

    def calculate_turnover_rate(self, stock_capital, old_reselected_stocks):
        """
        计算换手率
        """
        # 如果本月初仓位没变
        if self.current_time not in self.position_changed_time:
            self.lot_changed_rate[self.current_time] = 1.0
            return None
        # 得到候选股的手数
        _reselected_stocks_lot = pd.Series(0.0, stock_capital.index)
        for stock_id in stock_capital.index:
            open = self.get_price(stock_id, _open=True, _today=True)
            _reselected_stocks_lot[stock_id] = math.ceil(stock_capital[stock_id] / float(open))
        # 计算换手率, 如果是第一次换手就忽略
        if self.position_changed_time.index(self.current_time) != 0:
            if len(set(stock_capital.index)) != 0:
                _reserved = pd.Series()
                # get reserved stocks
                for _index in old_reselected_stocks.index:
                    if _index in _reselected_stocks_lot.index:
                        _reserved[_index] = min(_reselected_stocks_lot[_index], \
                                                old_reselected_stocks[_index])
                # calculate reserved capital
                _reserved_cap = 0
                _today_cap = 0
                for _index in _reserved.index:
                    _reserved_cap += _reserved[_index] * self.get_price(_index, _open=True, _today=True)
                for _index in old_reselected_stocks.index:
                    _today_cap += old_reselected_stocks[_index] * self.get_price(_index, _open=True, _today=True)
                if _today_cap != 0:
                    self.lot_changed_rate[self.current_time] = 1 - _reserved_cap / float(_today_cap)
                else:
                    self.lot_changed_rate[self.current_time] = 1.0
            else:
                self.lot_changed_rate[self.current_time] = 1.0

    def calculate_earning_rate(self):
        """
        """
        for _index, _time in enumerate(self.position_changed_time):
            if _index == 0:
                self.futures_earning_rate.append(1)
                self.stock_earning_rate.append(1)
                _s = self.timeList.tolist().index(_time)
                if len(self.position_changed_time) == 1:
                    self.futures_earning_rate.extend(get_earning_rate(self.data_futures['Close'].tolist()[_s:],
                                                                      self.data_futures['Close'].tolist()[_s])[1:])
                    self.stock_earning_rate.extend(
                        get_earning_rate(self.dynamic_record['capital'][_s:], self.dynamic_record['capital'][_s])[1:])
                    break
                _e = self.timeList.tolist().index(self.position_changed_time[_index + 1])
                self.futures_earning_rate.extend(get_earning_rate(self.data_futures['Close'].tolist()[_s:_e],
                                                                  self.data_futures['Close'].tolist()[_s])[1:])
                self.stock_earning_rate.extend(
                    get_earning_rate(self.dynamic_record['capital'][_s:_e], self.dynamic_record['capital'][_s])[1:])
            elif _index < len(self.position_changed_time) - 1:
                _s = self.timeList.tolist().index(_time)
                _e = self.timeList.tolist().index(self.position_changed_time[_index + 1])
                self.futures_earning_rate.append(
                    self.futures_earning_rate[-1] * self.data_futures['Close'].tolist()[_s] / \
                    float(self.data_futures['Close'].tolist()[_s - 1]))
                self.stock_earning_rate.append(self.stock_earning_rate[-1] * self.dynamic_record['capital'][_s] / \
                                               float(self.dynamic_record['capital'][_s - 1]))
                _futures_pre = self.futures_earning_rate[-2]
                _stock_pre = self.stock_earning_rate[-2]
                self.futures_earning_rate.extend([_v * _futures_pre for _v in \
                                                  get_earning_rate(self.data_futures['Close'].tolist()[_s:_e],
                                                                   self.data_futures['Close'].tolist()[_s - 1])[1:]])
                self.stock_earning_rate.extend([_v * _stock_pre for _v in \
                                                get_earning_rate(self.dynamic_record['capital'][_s:_e],
                                                                 self.dynamic_record['capital'][_s - 1])[1:]])
            else:
                _s = self.timeList.tolist().index(_time)
                self.futures_earning_rate.append(
                    self.futures_earning_rate[-1] * self.data_futures['Close'].tolist()[_s] / \
                    float(self.data_futures['Close'].tolist()[_s - 1]))
                self.stock_earning_rate.append(self.stock_earning_rate[-1] * self.dynamic_record['capital'][_s] / \
                                               float(self.dynamic_record['capital'][_s - 1]))
                _IF_pre = self.futures_earning_rate[-2]
                _stock_pre = self.stock_earning_rate[-2]
                self.futures_earning_rate.extend([_v * _IF_pre for _v in \
                                                  get_earning_rate(self.data_futures['Close'].tolist()[_s:],
                                                                   self.data_futures['Close'].tolist()[_s - 1])[1:]])
                self.stock_earning_rate.extend([_v * _stock_pre for _v in \
                                                get_earning_rate(self.dynamic_record['capital'][_s:],
                                                                 self.dynamic_record['capital'][_s - 1])[1:]])
        return self.stock_earning_rate, self.futures_earning_rate

    def get_real_time_list(self):
        """"""
        _s = self.timeList.tolist().index(self.position_changed_time[0])
        real_time_list = self.timeList.tolist()[_s:]
        return real_time_list

    def save_details(self):
        """
        1. 保存每次股票的交易细节:时间，股票id，状态，交易的几手，当前手数，当前价格，当前均价
        2. 保存计算动态收益的细节:时间，股票id，盈亏，当前时间的cash
        """
        stock_columns = ['time', 'stock', 'status', 'num', 'current_num', 'price', 'avg_price']
        self.details_of_deal = pd.DataFrame(np.array(self.stock_deal_details), columns=stock_columns)
        self.details_of_deal.to_csv('./data/output_data/stock_detal.csv', mode='a')
        capital_columns = ['time', 'id', 'lot', 'price', 'avg_price', 'earnings', 'curr_cash']
        self.details_of_calculate_capital = pd.DataFrame(np.array(self.calculate_capital_details),
                                                         columns=capital_columns)
        self.details_of_calculate_capital.to_csv('./data/output_data/calculate_capital_detail.csv', mode='a')

    def allocate_capital(self, active_capital, reselected_stocks, strategy=1):
        """
        选择资金分配方式
        """
        if self.resume_system == True:
            stock_capital = pd.Series()
            stock_capital = self._temp_data['Capital']
            stock_capital.index = self._temp_data['Code']
            # 系统备份数据
            self.sys_recorder.add_data(stock_capital, self.current_time)
            return stock_capital
        stock_capital = pd.Series(0.0, reselected_stocks)
        # 平均
        if strategy == 1:
            average_capital = 0.98 * active_capital / len(reselected_stocks)
            for _stock_id in reselected_stocks:
                stock_capital[_stock_id] = average_capital
        # 市值加权或者市值对数加权
        elif strategy == 2 or strategy == 3:
            _dict = {}
            for _stock_id, _value in self.market_capital.values:
                if _stock_id in reselected_stocks:
                    if strategy == 2:
                        _dict[_stock_id] = _value
                    else:
                        _dict[_stock_id] = math.log(_value)
            for _stock_id in reselected_stocks:
                stock_capital[_stock_id] = 0.98 * active_capital * _dict[_stock_id] / sum(_dict.values())
        else:
            pass
        # 系统备份数据
        self.sys_recorder.add_data(stock_capital, self.current_time)
        return stock_capital


def get_earning_rate(capital_record, base=None):
    """
    计算收益率的函数
    """
    _earning_rate = []
    for _index, _capital in enumerate(capital_record):
        if _index == 0:
            _earning_rate.append(0)
        elif base == None:
            _earning_rate.append(_capital / capital_record[_index - 1])
        else:
            _earning_rate.append(_capital / base)
    return _earning_rate


def get_alpha_rate(stock_rate, futures_rate, entirely_hedge=False):
    """
    """
    # 初始化，后续要通过切片剔除，不然会缺少stock_futures定义
    stock_futures = [1]
    # alpha_summary_rate用作在不换仓的时间段内alpha收益率的累积量
    alpha_summary_rate = 1
    # 得到每天的股票和股指期货的变化率
    stock_rate_everyday = [0] + [(_v - stock_rate[_i]) / float(stock_rate[_i]) for _i, _v in enumerate(stock_rate[1:])]
    futures_rate_everyday = [0] + [(_v - futures_rate[_i]) / float(futures_rate[_i]) for _i, _v in
                                   enumerate(futures_rate[1:])]
    for _i, _ in enumerate(stock_rate):
        # 求出当天股票收益率和当天期货收益率
        today_stock_rate = stock_rate_everyday[_i]
        if today_stock_rate == 0 and entirely_hedge is True:
            today_futures_rate = 0
        else:
            today_futures_rate = futures_rate_everyday[_i]
        # 求出当天股票收益率和当天期货收益率
        today_alpha_rate = 1 + today_stock_rate - today_futures_rate

        stock_futures.append(alpha_summary_rate * today_alpha_rate)
        alpha_summary_rate *= today_alpha_rate
        print "当天alpha收益率为%s" % (today_stock_rate - today_futures_rate)
        print "当天alpha累计收益率为%s" % stock_futures[-1]
    return stock_futures[1:]

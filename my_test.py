#!/usr/bin/env python
# coding=utf-8
import sys
import cPickle
import traceback

import matplotlib.pyplot as plt

import account
import demo_strategy
import strategy as st
import performance as mp
import pandas


profitRate =[]

if __name__ == '__main__':
    #将策略的一些信息用于初始化my_account
    my_account = account.Account(
                                st.start,
                                st.end,
                                st.freq,
                                st.universe_code,
                                st.capital_base,
                                st.short_capital,
                                st.benchmark,
                                st.self_defined)
    #获取基本数据
    my_account.iniData()
    #在my_account里面声明用户自定义变量
    st.initialize(my_account)
    #总的交易日天数
    day_num=len(my_account.timeList)
    try:
        for n,i in enumerate(my_account.timeList): #循环交易日，模拟交易
            my_account.last_time=my_account.current_time #注：第一天last_time为None
            my_account.current_time=i
            my_account.days_counts=n+1 #交易日计数器
            if n<day_num-1: #获取下一个交易日的日期
                my_account.tomorrow=my_account.timeList[n+1]
            my_account.order_temp=[] #新建临时保存今天交易记录的列表
            my_account.dynamic_record['trade_time'].append(i) #在动态记录里面添加今天的日期
            st.handle_data(my_account, strategy_func=demo_strategy.dde_func) #调用用于策略
            # print my_account.SD
            my_account.calculate_monthly_profit() #计算月收益
            my_account.dynamic_record['cash'].append(my_account.cash)
            my_account.dynamic_record['blotter'].append(my_account.order_temp)
            my_account.dynamic_record['capital'].append(my_account.calculate_capital(i))
            my_account.dynamic_record['futures'].append(my_account.calculate_futures(i))  # 计算做空股指期货的动态收益
            my_account.dynamic_record['alpha_capital'].append(my_account.dynamic_record['capital'][-1] + my_account.dynamic_record['futures'][-1])
            my_account.DR.loc[i,'cash'] = my_account.cash
            my_account.DR.loc[i,'blotter'] = cPickle.dumps(my_account.order_temp)  # 不可存储列表,可以序列化
            my_account.DR.loc[i,'capital'] = my_account.dynamic_record['capital'][-1]
            my_account.DR.loc[i,'futures'] = my_account.dynamic_record['futures'][-1]
            my_account.DR.loc[i,'alpha_capital'] = my_account.dynamic_record['alpha_capital'][-1]

            # 自己添加计算每日收益率并print出来的情况
            if len(profitRate) == 0:
                profitRate.append(my_account.dynamic_record['capital'][-1])
            profitRate.append(my_account.dynamic_record['capital'][-1])
            each_day_change_rate = (profitRate[-1]-profitRate[-2])/profitRate[-2]
            print "%s的收益率为%s" % (my_account.current_time, each_day_change_rate)

            #记录每日的股票的持股，收益情况
            my_account.calculate_stock_daily_hold_profit()
        
        # 添加end时间点进行股票池选取，此为预测明天选股的股票池
        # (不做买卖操作，不计算收益，只得出明天股票池,排除的股票只能用昨日的进行delete，因为今天情况属于未来数据)
        my_account.delete_exceptions(suspension=True, down=True, up=True, st=True)
        my_account.last_time = my_account.current_time
        end = pandas.to_datetime(st.end)
        my_account.current_time = end
        st.strategy(my_account, strategy_func=demo_strategy.dde_func) #调用用于策略
        
    except Exception, e:
        my_account.sys_recorder.back_up()
        etype, value, tb = sys.exc_info()
        traceback.print_exception(etype, value, tb)
        print "中断,第一部分,数据已保存,当前时间为:", my_account.current_time
    try:
        d = my_account.dynamic_record['capital']
        futures = my_account.dynamic_record['futures']
        # 得到时间列表
        real_time_list = my_account.get_real_time_list()
        #计算股票以及股指期货的收益率
        stock_rate, futures_rate = my_account.calculate_earning_rate()
        #alpha收益率
        stock_futures_rate = account.get_alpha_rate(stock_rate, futures_rate, entirely_hedge=False)
        #把alpha收益率添加到my_account类的属性中
        my_account.alpha_rate = zip(real_time_list, stock_futures_rate)
        #把stock收益率添加到my_account类的属性中
        my_account.stock_rate = zip(real_time_list, stock_rate)
        my_account.sum_ratio = zip(real_time_list, stock_rate, futures_rate, stock_futures_rate)
        # 保存数据
        my_account.save_details()
        fig2 = plt.figure('fig4')

        min_length = min(len(real_time_list), len(stock_rate), len(futures_rate), len(stock_futures_rate))
        real_time_list = real_time_list[len(real_time_list)-min_length:]
        stock_rate = stock_rate[len(stock_rate)-min_length:]
        futures_rate = futures_rate[len(futures_rate)-min_length:]
        stock_futures_rate = stock_futures_rate[len(stock_futures_rate)-min_length:]

        plt.title('stock, futures and alpha')
        plt.xlabel('time')
        plt.ylabel('capital')
        plt.plot(real_time_list, stock_rate, label='stock rate')
        plt.plot(real_time_list, futures_rate, label='%s rate' % my_account.benchmark_code)
        plt.plot(real_time_list, stock_futures_rate, label='alpha')
        plt.legend(loc=0)
        perf = mp.Performance(my_account,plt)
        perf.benchmark()
        perf.dynamic_rate()
        perf.calculate_ratio()
        perf.save_ratio(my_account.sum_ratio)
        plt.show()
    except Exception, e:
        etype, value, tb = sys.exc_info()
        traceback.print_exception(etype, value, tb)
        print "中断,第二部分,数据已保存,当前时间为:", my_account.current_time
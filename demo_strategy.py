#!/usr/bin/env python
# coding=utf-8
import pandas as pd
import MySQLdb
from PriorityQueue import PriorityQueue


def strategy_func(acc):
    """
    测试策略
    """
    reselected_stocks = []
    for stock_id in acc.active_universe:
        # 比较这个月第一个交易日的开盘价是否大于上个月最后一个交易日的收盘价
        if len(acc.getDailyHistory(stock_id, 1)['close'].values) == 0:
            continue
        elif acc.get_price(stock_id, _open=True, _today=True) > acc.getDailyHistory(stock_id, 1)['close'].values[-1]:
            # 把符合开盘价大于收盘价的股票放入一个新的列表
            reselected_stocks.append(stock_id)
    return reselected_stocks



con = MySQLdb.connect(host="192.168.0.114", charset="utf8", user="root", passwd="fit123456", db="pachong")
update_con = MySQLdb.connect(host="192.168.0.114", charset="utf8", user="root", passwd="fit123456", db="update")


def dde_func(acc):
    num = 10
    universe = acc.active_universe
    last_date = acc.last_time.to_datetime().date()

    # 上一个交易日dde大于０
    sql_dde = """select  wind_code from dde where date = "%s" and  dde_net is not null and dde_net >0  """ %str(last_date)
    df_dde = pd.read_sql(sql_dde,con=con)
    dde_code_list = df_dde['wind_code'].tolist()
    set_dde = set(dde_code_list)
    set_universe = set(universe)
    set_join = set_dde & set_universe
    if len(set_join) < num:
        return list(set_join)
    sql_market = """select distinct wind_code,mkt_cap_float from A_stock_mkt_cap_float where date ="%s" """ % str(last_date)
    df_market = pd.read_sql(sql_market, con=update_con)
    market_wind_list = df_market["wind_code"].tolist()
    market_value_list = df_market["mkt_cap_float"].tolist()
    dict_market = dict(zip(market_wind_list, market_value_list))
    list_join = list(set_join)
    queue = PriorityQueue()
    for wind_code in list_join:
        market_value = dict_market[wind_code]
        queue.put(wind_code, market_value)
    ret = queue.get_list(num)
    # print ret
    return ret
